/*
  This is a file of data and helper functions that we can expose and use in our templating function
*/

// FS is a built in module to node that let's us read files from the system we're running on
const fs = require('fs');

// moment.js is a handy library for displaying dates. We need this in our templates to display things like "Posted 5 minutes ago"
exports.moment = require('moment');

// Dump is a handy debugging function we can use to sort of "console.log" our data
exports.dump = (obj) => JSON.stringify(obj, null, 2);

// Making a static map is really long - this is a handy helper function to make one
exports.staticMap = ([lng, lat]) => `https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=14&size=800x150&key=${process.env.MAP_KEY}&markers=${lat},${lng}&scale=2`;

// inserting an SVG
exports.icon = (name) => fs.readFileSync(`./public/images/icons/${name}.svg`);

// Some details about the site
exports.siteName = `Watekes`;

exports.menu = [
  { slug: '/tags', title: 'Animadores', icon: 'animadores',
      'submenu':[
          {slug: '/payasos', title: 'Payasos'},
          {slug: '/magos', title: 'Magos'},
          {slug: '/imitadores', title: 'Imitadores'},
          {slug: '/botargas', title: 'Botargas'},
          {slug: '/pintacaritas', title: 'Pintacaritas'},
          {slug: '/acrobatas', title: 'Acrobatas'},
          {slug: '/bailarines', title: 'Bailarines'},
          {slug: '/performance', title: 'Performance'},
          {slug: '/fiesta-tematica', title: 'Fiesta Tematica'},
          {slug: '/strippers', title: 'Strippers'},
        ]
  },
  { slug: '/musica', title: 'Música', icon: 'musica',
      'submenu':[
          {slug: '/marachis', title: 'Mariachis'},
          {slug: '/banda', title: 'Banda'},
          {slug: '/dj', title: 'DJ'},
          {slug: '/grupo-musical', title: 'Grupo Musical'}
        ]
  },
  { slug: '/accesorios', title: 'Accesorios', icon: 'accesorios' },
  { slug: '/pasteles', title: 'Pasteles', icon: 'pastel' },
  { slug: '/renta', title: 'Renta', icon: 'renta' },
  { slug: '/comida', title: 'Comida', icon: 'comida' },
  { slug: '/foto-y-video', title: 'Foto y Video', icon: 'comida' },
  { slug: '/flores', title: 'Flores', icon: 'flores' },
  { slug: '/salones', title: 'Salones', icon: 'salones' },

  // { slug: '/stores', title: 'Animadores', icon: 'store', },
  // { slug: '/top', title: 'Top', icon: 'top', },
  // { slug: '/add', title: 'Agregar', icon: 'add', loggedIn : 1 },
  // { slug: '/map', title: 'Mapa', icon: 'map'  }
];
