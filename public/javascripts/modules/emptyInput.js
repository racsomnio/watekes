function emptyInput(form){
  if(form){
    form.addEventListener("blur", function(e) {
      var currClass = e.target.className;

      if (e.target.value != "") {
        if (!currClass.match(/notEmpty/gi)) {
          e.target.className += ((e.target.className) ? " " : "") + "notEmpty";
        }
      } else {
        if (currClass.match(/notEmpty/gi)) {
          e.target.className = e.target.className.replace('notEmpty', '');
        }
      }

    }, true);
  }
}

export default emptyInput;
