import { $ } from './bling';

function autocompleteWatekeLocation(input){
  if(!input) return;
  const options = {
      // types: ['(cities)'],
      types: ['geocode'],
      componentRestrictions: {country: 'mx'}
  }
  const dropdown = new google.maps.places.Autocomplete(input, options);

  dropdown.addListener('place_changed', () => {
    const place = dropdown.getPlace();
    // console.log(JSON.stringify(place));
    document.cookie= "wLat="+place.geometry.location.lat();
    document.cookie= "wLng="+place.geometry.location.lng();
    document.cookie= encodeURI(`wLoc=${place.address_components[0].long_name}, ${place.address_components[1].long_name}`);
    window.location.href = "/alrededor";
  })
};

export default autocompleteWatekeLocation;
