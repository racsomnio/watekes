function autocomplete(input, latInput, lngInput){
  // console.log(input, latInput, lngInput);
  if(!input) return;
  const options = {
    types: ['geocode'],
    componentRestrictions: {country: 'mx'}
  }
  const dropdown = new google.maps.places.Autocomplete(input, options);

  dropdown.addListener('place_changed', () => {
    const place = dropdown.getPlace();
    // console.log(JSON.stringify(place));
    latInput.value = place.geometry.location.lat();
    lngInput.value = place.geometry.location.lng();
  })

  // dont submit form when hitting Enter selection an address from autocomplete
  input.on('keydown', (event) => {
    if(event.keyCode === 13) event.preventDefault();
  })
}

export default autocomplete;
