import '../sass/style.scss';

import { $, $$ } from './modules/bling';
import autocomplete from './modules/autocomplete';
import emptyInput from './modules/emptyInput';
import typeAhead from './modules/typeAhead';
import makeMap from './modules/map';
import autocompleteWatekeLocation from './modules/jumbotron';
import ajaxHeart from './modules/heart';

autocompleteWatekeLocation($('#watekeAddress'));

autocomplete($('#address'), $('#lat'), $('#lng'));

emptyInput($('.form'));

typeAhead($('.search'));

makeMap( $('#map') );

const heartForms = $$('form.heart');
heartForms.on('submit', ajaxHeart);
