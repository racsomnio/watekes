const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;
// const md5 = require('md5'); // to make a hash
const validator = require('validator'); // validate fields
const mongodbErrorHandler = require('mongoose-mongodb-errors'); //get better error messages
const passportLocalMongoose = require('passport-local-mongoose');

const userSchema = new Schema({
  userType: {
    type: String
  },
  nombre: {
    type: String,
    required: 'Danos tu nombre',
    trim: true,
    lowercase: true
  },
  paterno: {
    type: String,
    required: 'Danos tu apellido paterno',
    trim: true,
    lowercase: true
  },
  materno: {
    type: String,
    trim: true,
    lowercase: true
  },
  sexo: {
    type: String,
    required: 'Selecciona Femenino o Masculino',
  },
  dd: {
    type: String,
  },
  mm: {
    type: String,
  },
  yyyy: {
    type: String,
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, 'El correo electrónico es incorrecto'],
    required: 'Por favor escribe tu correo electrónico'
  },

  resetPasswordToken: String,
  resetPasswordExpires: Date,
  hearts: [
    { type: mongoose.Schema.ObjectId, ref: 'Store' }
  ]
});

// userSchema.virtual('gravatar').get(function (){
//   const hash = md5(this.email);
//   return `https://gravatar.com/avatar/${hash}/?=s=200`;
// });

userSchema.plugin(passportLocalMongoose, { usernameField: 'email'});
userSchema.plugin(mongodbErrorHandler);

module.exports = mongoose.model('User', userSchema);
