const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');

const storeSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    lowercase: true,
    required: 'Escribe el nombre de un animador, grupo, o negocio'
  },
  slug: String,
  description: {
    type: String,
    trim: true
  },
  business: {
    ocupation: {
      type: String,
      lowercase: true
    },
    audienceType: [{
      type: String
    }],
    includes: {
      type: String
    },
    eventType:[{
      type: String
    }],
    audienceSize:[{
      type: String
    }],
    lang: [{
      type: String
    }],
    tags:[{
      type: String
    }],
    duration: {
      type: String
    }
  },
  payment:{
    price: {
      type: Number,
      required: 'Inserta el precio minimo de tus servicios.'
    },
    priceType: {
      type: String
    },
    paymentMethod:{
      type: String
    },
    upfront: {
      type: String
    }
  },
  contact: {
    fixPhone: {
      type: Number
    },
    whatsApp: {
      type: Number
    },
    website: {
      type: String
    },
    fb: {
      type: String
    },
    youtubeCh: {
      type: String
    }
  },
  created: {
    type: Date,
    default: Date.now
  },
  location: {
    type: {
      type: String,
      default: 'Point'
    },
    coordinates: [{
      type: Number,
      required: 'Por favor danos tus coordenadas'
    }],
    address: {
      type: String,
      required: 'Por favor inserta tu dirección'
    }
  },
  photos: [],
  youtubeVideo: [String],
  author: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: 'You must supply an author'
  }
},{
  toJSON: { virtuals: true }, // add this to add the virtual into the JSON or OBJECT
  toObject: { virtuals: true } // Otherwise is not attached
});

// Indexes
storeSchema.index({
  name: 'text',
  description: 'text'
});

storeSchema.index({ location: '2dsphere' });

storeSchema.pre('save', async function(next){
  console.log('pre saved');
  // if(!this.isModified('name')){
  //   return next(); //skip it and move to the next middleware
  // }
  // console.log(this);
  this.slug = slug(this.name);

  const slugRegEx = new RegExp( `^(${this.slug})((-[0-9]*$)?)$`, 'i' ); // modifie url for duplicated names
  const storesWithSlug = await this.constructor.find({ slug: slugRegEx }); //use constructor to look inside a model that is not yet there
  if(storesWithSlug.length){
    this.slug = `${this.slug}-${storesWithSlug.length + 1}`;
  }
  next();
});

storeSchema.pre('findOneAndUpdate', function(next){
  this._update.slug = slug(this._update.name);
  next();
});

storeSchema.statics.getTagList = function(){
  return this.aggregate([
    { $unwind: '$tags'},
    { $group: { _id: '$tags', count:{ $sum: 1 } }},
    { $sort: { count: -1 }}
  ])
};

storeSchema.statics.getTopStores = function(){
  return this.aggregate([
    // lookup stores and populate their reviews
    { $lookup: {
        from: 'reviews', localField: '_id', foreignField: 'store', as: 'reviews'
      }
    },
    // filter for only items that have 2 or more reviews
    { $match: { 'reviews.1': { $exists: true }}},
    // Add the average reviews field
    { $addFields: {
      // photo: '$$ROOT.photo',
      // name: '$$ROOT.name',
      // reviews: '$$ROOT.reviews',
      // slug: '$$ROOT.slug',
      averageRating: { $avg: '$reviews.rating'}
    }},
    // sort it by our new field, highest reviews first
    { $sort: { averageRating: -1 }},
    // limit to at most 10
    { $limit: 10 }
  ]);
};

// Find reviews where the stores _id === store property of review and populate it with vitual
storeSchema.virtual('reviews', {
  ref: "Review", // model to link
  localField: '_id', // field on the stores
  foreignField: 'store' // field on the review
});

function autopopulate(next){
  this.populate('reviews');
  next();
}

storeSchema.pre('find', autopopulate);
storeSchema.pre('findOne', autopopulate);

module.exports =  mongoose.model('Store', storeSchema);
