const passport = require('passport');
const mongoose = require('mongoose');
const User = mongoose.model('User');

passport.use(User.createStrategy());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// passport.serializeUser((obj, done) => {
//   if (obj instanceof Provider) {
//     done(null, { id: obj.id, type: 'Provider' });
//   } else {
//     done(null, { id: obj.id, type: 'User' });
//   }
// });
//
// passport.deserializeUser((obj, done) => {
//   if (obj.type === 'Provider') {
//     Provider.get(obj.id).then((provider) => done(null, provider));
//   } else {
//     User.get(obj.id).then((user) => done(null, user));
//   }
// });
