const nodemailer = require('nodemailer');
const pug = require('pug');
const juice = require('juice'); // to inline css
const htmlToText = require('html-to-text');
const promisify = require('es6-promisify');

const transport = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS
  }
});

const generateHTML = (filename, options = {}) => {
  const html = pug.renderFile(`${__dirname}/../views/email/${filename}.pug`, options);
  const inlined = juice(html);
  // console.log(html);
  return inlined;
}

exports.send = async (options) => {
    const html = generateHTML(options.filename, options);
    const text = htmlToText.fromString(html);
    const mailOptions = {
      from: `Watekes <noreply@watekes.com`,
      to: options.user.email,
      subject: options.subject,
      html,
      text
    };
    const sendMail = promisify(transport.sendMail, transport);
    return sendMail(mailOptions);
};

// Manual testing
// transport.sendMail({
//   from: 'Oscar Flores <oscar.uix@gmail.com>',
//   to: 'oscar.ui@watekes.com',
//   subject: 'hey, just testing this shit',
//   html: 'Hey, as you can <strong>see</strong> this works',
//   text: 'Hey, as you can see this works'
// })
