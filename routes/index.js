const express = require('express');
const router = express.Router();
const storeController = require('../controllers/store-controller');
const userController = require('../controllers/user-controller');
const authController = require('../controllers/auth-controller');
const reviewController = require('../controllers/review-controller');
const { catchErrors } = require('../handlers/errorHandlers');

router.get('/', storeController.homePage);
router.get('/alrededor', catchErrors(storeController.alrededor));
router.get('/alrededor/:ocupation', catchErrors(storeController.alrededor));
router.get('/alrededor/page/:page', catchErrors(storeController.alrededor));
router.get('/alrededor/:ocupation/page/:page', catchErrors(storeController.alrededor));
router.get('/register', userController.registerForm);
router.post('/register',
  userController.validateRegister,
  userController.register,
  authController.login
);
router.get('/cuenta', authController.isLoggedIn, userController.account);
router.post('/cuenta', catchErrors(userController.updateAccount));

router.get('/presenta/:slug', catchErrors(storeController.getStoreBySlug));

router.get('/stores', catchErrors(storeController.getStores));
router.get('/stores/page/:page', catchErrors(storeController.getStores));
router.get('/add', authController.isLoggedIn, storeController.addStore);
router.post('/add',
  storeController.upload,
  catchErrors(storeController.resize),
  catchErrors(storeController.createStore)
);
router.get('/presenta/:id/edit', catchErrors(storeController.editStore));
router.post('/add/:id',
  storeController.upload,
  catchErrors(storeController.resize),
  catchErrors(storeController.updateStore)
);

router.get('/tags', catchErrors(storeController.getStoresByTag));
router.get('/tags/:tag', catchErrors(storeController.getStoresByTag));

router.get('/login', userController.loginForm);
router.post('/login', authController.login)


router.get('/logout', authController.logout);

router.post('/account/forgot', catchErrors(authController.forgot));
router.get('/account/reset/:token', catchErrors(authController.reset));
router.post('/account/reset/:token',
  authController.confirmedPasswords,
  catchErrors(authController.update)
);

router.get('/hearts', authController.isLoggedIn, catchErrors(storeController.getHearts));
router.post('/reviews/:id',
  authController.isLoggedIn,
  catchErrors(reviewController.addReview)
);
router.get('/top', catchErrors(storeController.getTopStores));

/* API */
router.get('/api/search', catchErrors(storeController.searchStores));
router.get('/api/stores/near', catchErrors(storeController.mapStores));
router.post('/api/stores/:id/heart', catchErrors(storeController.heartStore));




module.exports = router;
