const passport =  require('passport');
const crypto = require('crypto');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const promisify = require('es6-promisify');
const mail = require('../handlers/mail');

exports.login = passport.authenticate('local', {
  failureRedirect: '/login',
  failureFlash: 'Hubo un error, intenta otra vez.',
  successRedirect: '/',
  // successFlash: 'Bienvenido!'
});

exports.logout = (req, res) => {
  req.logout();
  // req.flash('success', 'Vuelve pronto');
  res.redirect('/');
};

exports.isLoggedIn = (req, res, next) => {
  if(req.isAuthenticated()){
    return next();
  }
  req.flash('error', 'Por favor ingresa con tu correo y contraseña.')
  res.redirect('/login');
};

exports.forgot = async (req, res) => {
  // 1) See if user with that email exists
  const user = await User.findOne({ email: req.body.email });
  if(!user){
    req.flash('error', 'No existe una cuenta con ese correo electrónico.');
    return res.redirect('/login');
  }
  // 2) Set reset tokens and expiry on their account
  user.resetPasswordToken = crypto.randomBytes(20).toString('hex');
  user.resetPasswordExpires = Date.now() + 3600000; // 1hr from now
  await user.save();
  // 3) send email with tokens
  const resetURL = `http://${req.headers.host}/account/reset/${user.resetPasswordToken}`;
  await mail.send({
    user,
    subject: 'Restablecer Contraseña',
    resetURL,
    filename: 'password-reset'
  });
  req.flash('success', `Te hemos enviado un correo para que reinicies tu contraseña.`);
  // 4) redirect to login page
  res.redirect('/login');
};

exports.reset = async (req, res) => {
  const user = await User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  });
  if(!user){
    req.flash('error', 'Tu contraseña es incorrecta o tu tiempo para reiniciarla ha expirado.');
    return res.redirect('/login');
  }
  res.render('reset', { title: 'Reiniciar Contraseña' });
};

exports.confirmedPasswords = (req, res, next) => {
  if(req.body.password === req.body['password-confirm']){ //use [] to access els with dash (i.e. password-confirm)
    return next();
  }
  req.flash('error', 'Tus contraseñas no son iguales.');
  res.redirect('back');
};

exports.update = async (req, res) => {
  const user = await User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  });
  if(!user){
    req.flash('error', 'Tu contraseña es incorrecta o tu tiempo para reiniciarla ha expirado.');
    return res.redirect('/login');
  }
  const setPassword = promisify(user.setPassword, user);
  await setPassword(req.body.password);
  user.resetPasswordToken = undefined;
  user.resetPasswordExpires = undefined;
  const updateUser = await user.save();
  await req.login(updateUser);
  req.flash('success', 'Tu contraseña ha sido actualizada. Has ingresado de nuevo!');
  res.redirect('/');
};
