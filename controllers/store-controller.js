const mongoose = require('mongoose');
const Store = mongoose.model('Store');
const User = mongoose.model('User');
const multer = require('multer') // To handle images
const jimp = require('jimp'); // To resize images
const uuid = require('uuid'); // to add unique identifier

const multerOptions = {
  storage: multer.memoryStorage(),
  fileFilter(req, file, next) {
    const isPhoto = file.mimetype.startsWith('image/');
    if(isPhoto){
      next(null, true); //this means it works and keep going
    }else{
    next({ message: 'Solo se permiten imagenes, intentalo de nuevo con una imagen.'}, false)
    }
  }
}

exports.homePage = (req, res) => {
  res.render('index');
}

exports.addStore = (req, res) => {
  res.render('editStore', {title: 'Agregar Personaje'});
}

exports.upload = multer(multerOptions).array('photo', 15);

exports.resize = async (req, res, next) => {
  // console.log(req.files);
  if(!req.files){
    next(); //skip to next middleware
    return;
  }

  const jimps = [];
  const photosArr = [];

  req.files.map(file => {
    jimps.push(jimp.read(file.buffer));
  });
  req.body.photos = req.files.map(file => {
    const extension = file.mimetype.split('/')[1];
    const photoName = `${uuid.v4()}.${extension}`;
    return photoName;
  });
  const photos = Promise.all(jimps)
      .then(function(data){
        data.map(function(d, i){
          d.resize(800, jimp.AUTO);
          d.write(`./public/uploads/${req.body.photos[i]}`);
        });
      });
  next();


  // const promisePhotos = await req.files.map( async photo => {
  //   const extension = photo.mimetype.split('/')[1];
  //   photoName = `${uuid.v4()}.${extension}`;
  //   photosArr.push(photoName);
  //
  //   // const jimpPhoto = await jimp.read(photo.buffer);
  //   // await jimpPhoto.resize(800, jimp.AUTO);
  //   // await jimpPhoto.write(`./public/uploads/${photoName}`, err => console.error('write error: ', err)); // write photo to our filesystem
  // });
}

exports.createStore = async (req, res) => {
  // res.json(req)
  console.log(req.body);
  req.body.author = req.user._id;
  const store = await (new Store(req.body)).save();
  req.flash('success', `<strong>${store.name}</strong> ha sido publicado.`)
  res.redirect(`/presenta/${store.slug}`);
}

exports.getStores = async (req, res) => {
  const page = req.params.page || 1;
  const limit = 20;
  const skip = (page * limit) - limit;

  const storesPromise = await Store
    .find()
    .skip(skip)
    .limit(limit)
    .sort({ created: 'desc'});

  const countPromise = Store.count();
  const [stores, count] = await Promise.all([storesPromise, countPromise]);
  console.log(count);
  const pages = Math.ceil(count / limit);
  if(!stores.length && skip){
    req.flash('info', `Hey! You asked for page ${page}. But that doesnt exist. So i put you on page ${pages}`);
    res.redirect(`/stores/page/${pages}`);
    return;
  }
  res.render('stores', { title: 'Personajes', stores, page, pages, count })
}

const confirmOwner = (store, user) => {
  if(!store.author.equals(user._id)){
    throw Error('No se puede editar');
  }
};

exports.editStore = async (req, res) => {
  const store = await Store.findOne({ _id: req.params.id });
  // res.json(store)
  confirmOwner(store, req.user);
  res.render('editStore', { title: 'Editar Personaje', store})
}

exports.updateStore = async (req, res) => {
  req.body.location.type = 'Point';
  const store = await Store.findOneAndUpdate({ _id: req.params.id}, req.body, {
    new: true, //return the new store instead of the old one
    runValidators: true
  }).exec();
  req.flash('success', `El personaje <strong>${store.name}</strong> ha sido actualizado.`);
  res.redirect(`/presenta/${store._id}/edit`);

}

exports.getStoreBySlug = async (req, res, next) => {
  const store = await Store.findOne({ slug: req.params.slug }).populate('author reviews');
  if(!store) return next();
  res.render('store', {store, title: store.name})
}

exports.getStoresByTag = async (req, res) => {
  const tag = req.params.tag;
  const tagsPromise = Store.getTagList();
  const tagQuery = tag || { $exists: true}; // to show all stores when tag is not selected
  const storesPromise = Store.find({ tags: tagQuery });
  const [tags, stores] = await Promise.all([tagsPromise, storesPromise]);
  // res.json(stores);
  res.render('tags', {title:'Tags', tags, tag, stores});
}

exports.searchStores = async (req, res) => {
  const stores = await Store.find({
    $text: {
      $search: req.query.q
    }
  },{
      score: { $meta: 'textScore' }
  })
  .sort({
    score: { $meta: 'textScore' }
  })
  .limit(5);
  res.json(stores);
};

exports.mapStores = async (req, res) => {
  // mongo expects to pass first lng and the lat
  const coordinates = [req.query.lng, req.query.lat].map(parseFloat);
  const q = {
    location: {
      $near: {
        $geometry:{
          type: 'Point',
          coordinates
        },
        $maxDistance: 10000 //10km
      }
    }
  };
  const stores = await Store.find(q).select('slug name description location photo').limit(10);
  res.json(stores);
};

exports.alrededor = async (req, res) => {
  const ocupation = req.params.ocupation;
  const page = req.params.page || 1;
  const limit = 21;
  const skip = (page * limit) - limit;
  const cookie = req.cookies;
  const ocupationQuery = ocupation || '';
  // mongo expects to pass first lng and the lat
  const coordinates = [cookie.wLng, cookie.wLat].map(parseFloat);
  const q = {
    location: {
      $near: {
        $geometry:{
          type: 'Point',
          coordinates
        },
        $maxDistance: 50000 //10km
      }
    },
    'business.ocupation' : {$regex: ocupationQuery, $options: 'i'}
  };

  const storesPromise = await Store
                    .find(q)
                    .select('slug name location photos payment business')
                    .skip(skip)
                    .limit(limit);

  const categoriesPromise = await Store
                    .find(q)
                    .select('business.ocupation');

  const countPromise = await Store
                    .find(q)
                    .select('slug name location photos payment business')
                    .count();

  const [stores, categories, count] = await Promise.all([storesPromise, categoriesPromise, countPromise]);

  const pages = Math.ceil(count / limit);

  if(!stores.length && skip){
    req.flash('info', `Hey! You asked for page ${page}. But that doesnt exist. So i put you on page ${pages}`);
    res.redirect(`/stores/page/${pages}`);
    return;
  }
  category = categories.reduce((countMap, item) => {
    countMap[item.business.ocupation] = ++countMap[item.business.ocupation] || 1;
    return countMap;
  }, {});

  if(!ocupation){
    res.cookie('menu', JSON.stringify(category));
  }

  // res.json(stores)
  res.render('stores', {location: cookie.wLoc, stores, page, pages, categoryCookie: cookie.menu, category, ocupation, href: '/alrededor'})

};

exports.heartStore = async (req, res) => {
  const hearts = req.user.hearts.map(obj => obj.toString());
  const operator = hearts.includes(req.params.id) ? '$pull' : '$addToSet';
  const user = await User
    .findByIdAndUpdate(req.user._id,
    { [operator]: {hearts: req.params.id }},
    { new: true }
  );
  res.json(user);
};

exports.getHearts = async (req, res) => {
  const stores = await Store.find({
    _id: { $in: req.user.hearts }
  });
  res.render('stores', { title: 'Favoritos', stores });
};

exports.getTopStores = async (req, res) => {
  const stores = await Store.getTopStores();
  // res.json(stores);
  res.render('topStores', { stores, title: '★ Los mejores!' });
};
