const mongoose = require('mongoose');
const User = mongoose.model('User');
const promisify = require('es6-promisify');

exports.loginForm = (req, res) => {
  res.render('login', {title: 'Ingresar'});
}

exports.registerForm = (req, res) => {
  res.render('register', {title: 'Únete a la fiesta'});
}

exports.validateRegister = (req, res, next) => {
  console.log(req.body);
  req.sanitizeBody('nombre');
  req.checkBody('nombre', 'Por favor escribe tu nombre').notEmpty();
  req.checkBody('paterno', 'Por favor escribe tu apellido paterno').notEmpty();
  req.sanitizeBody('paterno');
  req.sanitizeBody('materno');
  req.checkBody('email', 'El correo electrónico es incorrecto').isEmail();
  req.sanitizeBody('email').normalizeEmail({
    remove_dots: false,
    remove_extension: false,
    gmail_remove_subaddress: false
  });
  req.checkBody('password', 'Escribe cual sera tu contraseña').notEmpty();
  req.checkBody('password-confirm', 'Para confirmar, por favor escribe de nuevo tu contraseña').notEmpty();
  req.checkBody('password-confirm', 'Tus contraseñas son diferentes').equals(req.body.password);

  const errors = req.validationErrors();
  if(errors){
    req.flash('error', errors.map(err => err.msg));
    res.render('register',{title: 'Registrarse', body: req.body, flashes: req.flash()} );
    return;
  }
  next();
};

exports.register = async (req, res, next) => {
  const user = new User({
                          userType: req.body.userType,
                          nombre: req.body.nombre,
                          paterno: req.body.paterno,
                          materno: req.body.materno,
                          sexo: req.body.sexo,
                          dd: req.body.dd,
                          mm: req.body.mm,
                          yyyy: req.body.yyyy,
                          email: req.body.email,
                        });
  const register = promisify(User.register, User);
  await register(user, req.body.password);
  next();
};

exports.account = (req, res) => {
  res.render('account', {title: 'Editar Cuenta' })
}
 exports.updateAccount = async (req, res) => {
   const updates = {
     nombre: req.body.nombre,
     paterno: req.body.paterno,
     materno: req.body.materno,
     cp: req.body.cp,
     email: req.body.email
   };
   const user = await User.findOneAndUpdate(
     { _id: req.user._id },
     { $set: updates },
     { new:true, runValidators: true, context: 'query' }
   );
   req.flash('success', 'Tu cuenta ha sido actualizada')
   res.redirect('back');
 };
